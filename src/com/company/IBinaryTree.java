package com.company;

/**
 * Created by christian on 17/12/15.
 */
public interface IBinaryTree<T> {
    boolean search(T obj);
    void insert(T obj);
    int size();
    boolean isEmpty();

    void indorder();
    void postorder();
    void preorder();
    boolean bfs(T obj);
    void dfs();
}
