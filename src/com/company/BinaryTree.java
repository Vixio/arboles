package com.company;

import java.util.ArrayDeque;
import java.util.LinkedList;

/**
 * Created by christian on 17/12/15.
 */
public class BinaryTree<T extends Comparable> implements IBinaryTree<T> {
    class Node<T extends Comparable> implements Comparable< T > {
        private T value;
        private Node<T> left;
        private Node<T> right;

        public Node(T value) {
            this.value = value;
        }

        public T getValue() {
            return value;
        }

        public Node<T> getLeft() {
            return left;
        }

        public Node<T> getRight() {
            return right;
        }

        public void setLeft(Node<T> left) {
            this.left = left;
        }

        public void setRight(Node<T> right) {
            this.right = right;
        }

        @Override
        public int compareTo(T o) {
            return o.compareTo(value); //value.compareTo(o);
        }

        @Override
        public String toString() {
            return "" + value;
        }
    }

    private Node<T> root;

    public BinaryTree() {
        this.root = null;
    }

    @Override
    public boolean search(T obj) {
        return search(root, obj);
    }

    private boolean search(Node<T> node, T target) {
        if ( node == null ) {
            return false;
        }

        if ( node.getValue().equals(target) ) {
            return true;
        }

        if ( node.compareTo(target) < 0 ) {
            return search(node.getLeft(), target);
        } else {
            return search(node.getRight(), target);
        }
    }

    @Override
    public void insert(T obj) {
        root = insert(root, obj);
    }

    private Node<T> insert(Node<T> node, T value) {
        if ( node == null ) {
            node = new Node<>(value);
        } else {
            if ( node.compareTo(value) < 0 ) {
                node.setLeft(insert(node.getLeft(), value));
            } else {
                node.setRight(insert(node.getRight(), value));
            }
        }

        return node;
    }

    @Override
    public int size() {
        return size(root);
    }

    private int size(Node<T> node) {
        if ( node == null ) {
            return 0;
        }

        return 1 + size(node.getLeft()) + size(node.getRight());
    }

    @Override
    public boolean isEmpty() {
        return false;
    }

    @Override
    public void indorder() {
        inorder(root);
        System.out.println();
    }

    private void inorder(Node<T> node) {
        if ( node != null ) {
            inorder(node.getLeft());
            System.out.println(node.getValue());
            inorder(node.getRight());
        }
    }

    @Override
    public void postorder() {
        postorder(root);
        System.out.println();
    }

    private void postorder(Node<T> node) {
        if ( node != null ) {
            postorder(node.getLeft());
            postorder(node.getRight());
            System.out.println(node.getValue());
        }
    }

    @Override
    public void preorder() {
        preorder(root);
        System.out.println();
    }

    private void preorder(Node<T> node) {
        if ( node != null ) {
            System.out.println(node.getValue());
            preorder(node.getLeft());
            preorder(node.getRight());
        }
    }

    @Override
    public boolean bfs(T elem) {
        ArrayDeque<Node<T>> q = new ArrayDeque<>();
        q.offer(root);

        while(!q.isEmpty()) {
            Node<T> node = q.poll();

            if ( node.getValue().equals(elem) ) {
                return true;
            }

            if ( node.getLeft() != null ) {
                q.offer(node.getLeft());
            }

            if ( node.getRight() != null ) {
                q.offer(node.getRight());
            }
        }

        return false;
    }

    @Override
    public void dfs() {
        dfs(root);
    }

    private void dfs(Node<T> node){
        if ( node == null ) {
            return;
        }

        System.out.println(node.getValue());

        dfs(node.getLeft());
        dfs(node.getRight());
    }

    private Node<T> findNode(Node<T> node, T target) {
        if ( node == null ) {
            return null;
        }

        if ( node.getValue().equals(target) ) {
            return node;
        }

        if ( node.compareTo(target) < 0 ) {
            return findNode(node.getLeft(), target);
        } else {
            return findNode(node.getRight(), target);
        }
    }

    public int getNumberOfChilds(T n) {
        Node<T> node = findNode(root, n);
        return size(node.getLeft()) + size(node.getRight());
    }

    public T getParent(T n) {
        if ( root.getValue() == n ) {
            return null;
        }

        return getParent(n, root);
    }

    private T getParent(T n, Node<T> node) {
        if ( node.getLeft() != null && node.getLeft().getValue().equals(n)) {
            return node.getValue();
        }

        if ( node.getRight() != null && node.getRight().getValue().equals(n)) {
            return node.getValue();
        }

        if ( node.getValue().compareTo(n) >= 0 ) {
            return getParent(n, node.getLeft());
        } else {
            return getParent(n, node.getRight());
        }
    }

    public int balance() {
        return size(root.getLeft()) - size(root.getRight());
    }

    public LinkedList<T> getNodesInGeneration(int generation) {
        return getNodesInGeneration(0, generation, root);
    }

    private LinkedList<T> getNodesInGeneration(int currentGen, int targetGen, Node<T> node) {
        if ( node == null || currentGen > targetGen ) {
            return new LinkedList<>();
        }

        if ( currentGen == targetGen ) {
            LinkedList<T> res = new LinkedList<>();
            res.add(node.getValue());
            return res;
        }

        LinkedList<T> elems1 = getNodesInGeneration(currentGen + 1, targetGen, node.getLeft());
        LinkedList<T> elems2 = getNodesInGeneration(currentGen + 1, targetGen, node.getRight());

        elems1.addAll(elems2);

        return elems1;
    }

    public T getHigherValue() {
        return getHigherValue(root);
    }

    public T getHigherValue(Node<T> node) {
        if ( node.getRight() != null ) {
            return getHigherValue(node.getRight());
        }

        return node.getValue();
    }

    public Double getAvg() {
        return sum(root) / size(root);
    }

    private Double sum(Node<T> node) {
        if ( node == null ) {
            return 0.0;
        }

        Double res = sum(node.getLeft());
        res += sum(node.getRight());

        if ( node.getValue() instanceof Double ) {
            res += (Double)node.getValue();
        }
        else if ( node.getValue() instanceof Integer ) {
            res += (Integer)node.getValue();
        }

        return res;
    }
}
