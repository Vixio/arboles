package com.company;

public class Main {
    //        20
    //   10       30
    // 5   15       35
    public static void main(String[] args) {
	    BinaryTree<Integer> a = new BinaryTree<>();
        a.insert(20);
        a.insert(10);
        a.insert(30);
        a.insert(5);
        a.insert(35);
        a.insert(15);

        System.out.println("----------");
        System.out.println("Preorder");
        System.out.println("----------");
        a.preorder();

        System.out.println("----------");
        System.out.println("Inorder");
        System.out.println("----------");
        a.indorder();

        System.out.println("----------");
        System.out.println("Postorder");
        System.out.println("----------");
        a.postorder();

        System.out.println("----------");
        System.out.println("BFS");
        System.out.println("----------");
        if ( a.bfs(30) ) {
            System.out.println("Se encontro el elemento 30");
        } else {
            System.out.println("No se encontro el elemento 30");
        }

        System.out.println("----------");
        System.out.println("DFS");
        System.out.println("----------");
        a.dfs();

        System.out.println("----------");
        System.out.println("----------");

        System.out.println("Tamaño: " + a.size());
        System.out.println("Numero de hijos del 20: " + a.getNumberOfChilds(20));
        System.out.println("Obtener padre de 10: " + a.getParent(10));
        System.out.println("Balance: " + a.balance());
        System.out.println("Nodos en la generacion 1: " + a.getNodesInGeneration(1));
        System.out.println("Promedio: " + a.getAvg());
    }
}
